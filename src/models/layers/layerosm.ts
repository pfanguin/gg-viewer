import Layer from "./layer";

class LayerOsm extends Layer {

  constructor(elem: any, order: number) {
    super(elem, order);
  }
}

export default LayerOsm;