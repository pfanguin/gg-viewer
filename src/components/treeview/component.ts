import GirafeResizableElement from '../../base/GirafeResizableElement'
import BaseLayer from '../../models/layers/baselayer';
import GroupLayer from '../../models/layers/layergroup';
import LayerManager from '../../tools/layermanager';
import LayerWms from '../../models/layers/layerwms';

class TreeViewComponent extends GirafeResizableElement {

  templateUrl = './template.html';
  styleUrl = './style.css';

  layerManager: LayerManager;

  isAllExpanded: boolean = false;
  areAllLegendExpanded: boolean = true;

  get isSwiperVisible() {
    return (this.state.layers.swipedLayers.left.length > 0 || this.state.layers.swipedLayers.right.length > 0);
  }

  constructor() {
    super('treeview');

    this.layerManager = LayerManager.getInstance();
  }

  registerEvents() {
    this.stateManager.subscribe('selectedTheme', () => this.onThemeChanged());
    this.stateManager.subscribe('layers\.layersList', () => super.render());
    this.stateManager.subscribe('layers\.swipedLayers', () => super.render());
    this.stateManager.subscribe('treeview\.advanced', () =>  super.render());
  }

  onThemeChanged() {
    if (this.state.selectedTheme != null) {
      this.state.layers.layersList = [...this.state.selectedTheme.layersTree];
      this.activateDefaultLayers(this.state.layers.layersList);
    }
    else {
      this.state.layers.layersList = [];
    }
  }

  activateDefaultLayers(layers: BaseLayer[]) {
    for (const layer of layers) {
      this.layerManager.activateIfDefaultChecked(layer);
      if (layer instanceof LayerWms) {
        this.layerManager.initializeLegends(layer);
      }
      if (layer instanceof GroupLayer) {
        this.activateDefaultLayers(layer.children);
      }
    }
  }

  connectedCallback() {
    this.loadConfig()
      .then(() => {
        this.render();
        super.girafeTranslate();
        this.registerEvents();
      });
  }

  render() {
    super.render();
    this.activateTooltips(false, [800, 0], 'right');
  }

  expandAll() {
    this.isAllExpanded = !this.isAllExpanded;
    this.#expandAllRecursive(this.state.layers.layersList);
    super.render();
  }

  #expandAllRecursive(layers: BaseLayer[]) {
    for (const layer of layers) {
      if (layer instanceof GroupLayer) {
        layer.isExpanded = this.isAllExpanded;
        this.#expandAllRecursive(layer.children);
      }
    }
  }

  toggleAllLegends() {
    this.areAllLegendExpanded = !this.areAllLegendExpanded;
    this.#toggleAllLegendsRecursive(this.state.layers.layersList);
    super.render();
  }

  #toggleAllLegendsRecursive(layers: BaseLayer[]) {
    for (const layer of layers) {
      if (layer instanceof LayerWms && layer.legend) {
        layer.isLegendExpanded = this.areAllLegendExpanded;
      }
      else if (layer instanceof GroupLayer) {
        this.#toggleAllLegendsRecursive(layer.children);
      }
    }
  }

  removeAll() {
    for (const layer of this.stateManager.state.layers.layersList) {
      this.layerManager.toggle(layer, 'off');
    }
    this.hideSwipe();
    this.state.layers.layersList = [];
    this.state.selectedTheme = null;
  }

  hideSwipe() {
    this.state.layers.swipedLayers = { left:[], right:[] };
    super.render();
  }
}

customElements.define('girafe-tree-view', TreeViewComponent);

export default TreeViewComponent;
